CREATE DATABASE IF NOT EXISTS jspservlet CHARACTER SET utf8 COLLATE utf8_general_ci;

USE jspservlet;

CREATE TABLE IF NOT EXISTS `role` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `code` VARCHAR(255) NOT NULL,
    `createdDate` TIMESTAMP,
    `modifiedDate` TIMESTAMP,
    `createdBy` VARCHAR(255),
    `modifiedBy` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS `user` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `userName` VARCHAR(150) NOT NULL,
    `password` VARCHAR(150) NOT NULL,
    `fullName` VARCHAR(150),
    `status` INT NOT NULL,
    `roleId` BIGINT NOT NULL,
    `createdDate` TIMESTAMP,
    `modifiedDate` TIMESTAMP,
    `createdBy` VARCHAR(255),
    `modifiedBy` VARCHAR(255),
    CONSTRAINT `fk_user_role` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
);

CREATE TABLE IF NOT EXISTS `category` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `code` VARCHAR(255) NOT NULL,
    `createdDate` TIMESTAMP,
    `modifiedDate` TIMESTAMP,
    `createdBy` VARCHAR(255),
    `modifiedBy` VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS `news` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255),
    `thumbnail` VARCHAR(255),
    `shortDescription` TEXT,
    `content` TEXT,
    `categoryId` BIGINT NOT NULL,
    `createdDate` TIMESTAMP,
    `modifiedDate` TIMESTAMP,
    `createdBy` VARCHAR(255),
    `modifiedBy` VARCHAR(255),
    CONSTRAINT `fk_news_category` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`)
);

CREATE TABLE IF NOT EXISTS `comment` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `content` TEXT NOT NULL,
    `userId` BIGINT NOT NULL,
    `newsId` BIGINT NOT NULL,
    `createdDate` TIMESTAMP,
    `modifiedDate` TIMESTAMP,
    `createdBy` VARCHAR(255),
    `modifiedBy` VARCHAR(255),
    CONSTRAINT `fk_comment_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
    CONSTRAINT `fk_comment_news` FOREIGN KEY (`newsId`) REFERENCES `news` (`id`)
);

insert into `category` (`code`, `name`) values
('the-thao', 'Thể thao'),
('the-gioi', 'Thế giới'),
('chinh-tri', 'Chính trị'),
('thoi-su', 'Thời sự'),
('goc-nhin', 'Góc nhìn');

insert into `news` (`title`, `categoryId`) values
('bài viết 1', 1),
('bài viết 2', 1),
('bài viết 3', 2);