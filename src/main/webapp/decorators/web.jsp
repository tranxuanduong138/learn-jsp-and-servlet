<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ include file="/common/taglib.jsp" %>
<html>
<head>
    <title><dec:title default="Trang chủ" /></title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/template/web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/template/web/css/shop-homepage.css" rel="stylesheet">
</head>
<body>
    <!-- header -->
    <%@ include file="/common/web/header.jsp" %>
    <!-- header -->

    <div class="container">
        <dec:body/>
    </div>

    <!-- footer -->
    <%@ include file="/common/web/footer.jsp" %>
    <!-- footer -->

    <!-- Bootstrap core JavaScript -->
    <script src="${pageContext.request.contextPath}/template/web/vendor/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/template/web/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
