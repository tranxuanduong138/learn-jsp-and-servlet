<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>

<html>
<head>
    <title>Danh sách bài viết</title>
</head>
<body>
    <div class="main-content">
        <form action="<c:url value="/admin-news" />" id="form-submit" method="get">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Trang chủ</a>
                        </li>
                    </ul><!-- /.breadcrumb -->
                </div>
                <div class="page-content">
                    <div class="row" >
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Tên bài viết</th>
                                        <th>Mô tả ngắn</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${model.resultList}">
                                        <tr>
                                            <td>${item.title}</td>
                                            <td>${item.shortDescription}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <ul class="pagination" id="pagination"></ul>
                                <input type="hidden" id="current-page" name="currentPage" value=""/>
                                <input type="hidden" id="items-per-page" name="itemsPerPage" value=""/>
                                <input type="hidden" id="sort-name" name="sortName" value=""/>
                                <input type="hidden" id="sort-by" name="sortBy" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.main-content -->

    <script type="text/javascript">
        var currentPage = ${model.currentPage};
        var totalPages = ${model.totalPages};
        var itemsPerPage = 2;
        $(function () {
            window.pagObj = $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: 10,
                startPage: currentPage,
                onPageClick: function (event, page) {
                    if (currentPage !== page) {
                        $('#items-per-page').val(itemsPerPage);
                        $('#current-page').val(page);
                        $('#sort-name').val('title');
                        $('#sort-by').val('desc');
                        $('#form-submit').submit();
                    }
                }
            }).on('page', function (event, page) {
                console.info(page + ' (from event listening)');
            });
        });
    </script>
</body>
</html>