package org.example.dao.impl;

import org.example.dao.ICategoryDAO;
import org.example.mapper.CategoryMapper;
import org.example.model.CategoryModel;

import java.util.List;

public class CategoryDAO extends AbstractDAO<CategoryModel> implements ICategoryDAO {
    @Override
    public List<CategoryModel> findAll() {
        String sql = "SELECT * FROM category";
        return query(sql, new CategoryMapper());
    }
}
