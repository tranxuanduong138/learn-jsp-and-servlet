package org.example.dao.impl;

import org.example.dao.INewsDAO;
import org.example.mapper.NewsMapper;
import org.example.model.NewsModel;
import org.example.paging.Pageable;

import java.util.List;

public class NewsDAO extends AbstractDAO<NewsModel> implements INewsDAO {
    @Override
    public NewsModel findOne(Long id) {
        String sql = "SELECT * FROM news WHERE id = ?";
        List<NewsModel> newsModelList = query(sql, new NewsMapper(), id);
        return newsModelList.isEmpty() ? null : newsModelList.get(0);
    }

    @Override
    public List<NewsModel> findAll(Pageable pageable) {
        StringBuilder sql = new StringBuilder("SELECT * FROM news");
        if (pageable.getSorter() != null) {
            sql.append(" ORDER BY " + pageable.getSorter().getSortName() + " " + pageable.getSorter().getSortBy());
        }
        if (pageable.getOffset() != null && pageable.getLimit() != null) {
            sql.append(" LIMIT " + pageable.getOffset() + ", " + pageable.getLimit());
        }
        return query(sql.toString(), new NewsMapper());
    }

    @Override
    public List<NewsModel> findByCategoryId(Long categoryId) {
        String sql = "SELECT * FROM news WHERE categoryId = ?";
        return query(sql, new NewsMapper(), categoryId);
    }

    @Override
    public Long save(NewsModel newsModel) {
        StringBuilder sql = new StringBuilder("INSERT INTO news");
        sql.append("(title, content, thumbnail, shortDescription, categoryId, " +
                "createdBy, createdDate) ");
        sql.append("VALUES (?, ?, ?, ?, ?, ?, ?)");
        return insert(sql.toString(),
                newsModel.getTitle(),
                newsModel.getContent(),
                newsModel.getThumbnail(),
                newsModel.getShortDescription(),
                newsModel.getCategoryId(),
                newsModel.getCreatedBy(),
                newsModel.getCreatedDate());
    }

    @Override
    public void update(NewsModel updatedNews) {
        StringBuilder sql = new StringBuilder("UPDATE news SET ");
        sql.append("title = ?, ");
        sql.append("content = ?, ");
        sql.append("thumbnail = ?, ");
        sql.append("shortDescription = ?, ");
        sql.append("categoryId = ?, ");
        sql.append("createdBy = ?, ");
        sql.append("createdDate = ?, ");
        sql.append("modifiedBy = ?, ");
        sql.append("modifiedDate = ? ");
        sql.append("WHERE id = ?");
        update(sql.toString(),
                updatedNews.getTitle(),
                updatedNews.getContent(),
                updatedNews.getThumbnail(),
                updatedNews.getShortDescription(),
                updatedNews.getCategoryId(),
                updatedNews.getCreatedBy(),
                updatedNews.getCreatedDate(),
                updatedNews.getModifiedBy(),
                updatedNews.getModifiedDate(),
                updatedNews.getId());
    }

    @Override
    public void delete(long id) {
        String sql = "DELETE FROM news WHERE id = ?";
        update(sql, id);
    }

    @Override
    public int getTotalItems() {
        String sql = "SELECT COUNT(*) FROM news";
        return count(sql);
    }
}
