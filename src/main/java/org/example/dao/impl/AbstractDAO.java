package org.example.dao.impl;

import org.example.dao.GenericDAO;
import org.example.mapper.RowMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AbstractDAO<T> implements GenericDAO<T> {
    public Connection getConnection() {
        try {
            // Load Driver
            Class.forName("com.mysql.jdbc.Driver");
            // Get connection
            String url = "jdbc:mysql://localhost:3306/jspservlet";
            String username = "root";
            String password = "passlagi#1";
            return DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<T> query(String sql, RowMapper<T> rowMapper, Object... parameters) {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(sql);
            setParameters(ps, parameters);
            rs = ps.executeQuery();
            List<T> results = new ArrayList<>();
            while (rs.next()) {
                results.add(rowMapper.mapRow(rs));
            }
            return results;
        } catch (Exception e) {
            return null;
        } finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { ps.close(); } catch (Exception e) { /* ignored */ }
            try { conn.close(); } catch (Exception e) { /* ignored */ }
        }
    }

    private void setParameters(PreparedStatement ps, Object... parameters) throws SQLException {
        for (int i = 0; i < parameters.length; i++) {
            Object o = parameters[i];
            int index = i + 1;
            if (o instanceof Long) {
                ps.setLong(index, (Long) o);
            }
            else if (o instanceof String) {
                ps.setString(index, (String) o);
            }
            else if (o instanceof Integer) {
                ps.setInt(index, (Integer) o);
            }
            else if (o instanceof Timestamp) {
                ps.setTimestamp(index, (Timestamp) o);
            }
            else if (o == null) {
                ps.setNull(index, Types.NULL);
            }
        }
    }

    @Override
    public void update(String sql, Object... parameters) {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        try {
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(sql);
            setParameters(ps, parameters);
            ps.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception e1) {
                /* ignored */
            }
        } finally {
            try { ps.close(); } catch (Exception e) { /* ignored */ }
            try { conn.close(); } catch (Exception e) { /* ignored */ }
        }
    }

    @Override
    public Long insert(String sql, Object... parameters) {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Long id = null;
        try {
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            setParameters(ps, parameters);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            conn.commit();
            return id;
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception e1) {
                /* ignored */
            }
            finally {
                return null;
            }
        } finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { ps.close(); } catch (Exception e) { /* ignored */ }
            try { conn.close(); } catch (Exception e) { /* ignored */ }
        }
    }

    @Override
    public int count(String sql, Object... parameters) {
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int count = 0;
            ps = conn.prepareStatement(sql);
            setParameters(ps, parameters);
            rs = ps.executeQuery();
            List<T> results = new ArrayList<>();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            return count;
        } catch (Exception e) {
            return 0;
        } finally {
            try { rs.close(); } catch (Exception e) { /* ignored */ }
            try { ps.close(); } catch (Exception e) { /* ignored */ }
            try { conn.close(); } catch (Exception e) { /* ignored */ }
        }
    }
}
