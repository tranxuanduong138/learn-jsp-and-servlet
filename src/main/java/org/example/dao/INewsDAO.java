package org.example.dao;

import org.example.model.NewsModel;
import org.example.paging.Pageable;

import java.util.List;

public interface INewsDAO extends GenericDAO<NewsModel> {
    NewsModel findOne(Long id);
    List<NewsModel> findAll(Pageable pageable);
    List<NewsModel> findByCategoryId(Long categoryId);
    Long save(NewsModel newsModel);
    void update(NewsModel updatedNews);
    void delete(long id);
    int getTotalItems();
}
