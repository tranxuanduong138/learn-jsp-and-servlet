package org.example.mapper;

import org.example.model.NewsModel;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewsMapper implements RowMapper<NewsModel> {
    @Override
    public NewsModel mapRow(ResultSet rs) throws SQLException {
        NewsModel news = new NewsModel();
        news.setId(rs.getLong("id"));
        news.setTitle(rs.getString("title"));
        news.setContent(rs.getString("content"));
        news.setThumbnail(rs.getString("thumbnail"));
        news.setShortDescription(rs.getString("shortDescription"));
        news.setCategoryId(rs.getLong("categoryId"));
        news.setCreatedBy(rs.getString("createdBy"));
        news.setCreatedDate(rs.getTimestamp("createdDate"));
        if (rs.getString("modifiedBy") != null) {
            news.setModifiedBy(rs.getString("modifiedBy"));
        }
        if (rs.getTimestamp("modifiedDate") != null) {
            news.setModifiedDate(rs.getTimestamp("modifiedDate"));
        }
        return news;
    }
}
