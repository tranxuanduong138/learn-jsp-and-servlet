package org.example.mapper;

import org.example.model.CategoryModel;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements RowMapper<CategoryModel> {
    @Override
    public CategoryModel mapRow(ResultSet rs) throws SQLException {
        CategoryModel category = new CategoryModel();
        category.setId(rs.getLong("id"));
        category.setName(rs.getString("name"));
        category.setCode(rs.getString("code"));
        return category;
    }
}
