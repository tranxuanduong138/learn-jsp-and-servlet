package org.example.service.impl;

import org.example.dao.ICategoryDAO;
import org.example.dao.impl.CategoryDAO;
import org.example.model.CategoryModel;
import org.example.service.ICategoryService;

import javax.inject.Inject;
import java.util.List;

public class CategoryService implements ICategoryService {
//    private ICategoryDAO categoryDAO;
//
//    public CategoryService() {
//        categoryDAO = new CategoryDAO();
//    }
    @Inject
    private ICategoryDAO categoryDAO;

    @Override
    public List<CategoryModel> findAll() {
        return categoryDAO.findAll();
    }
}
