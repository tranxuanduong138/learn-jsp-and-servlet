package org.example.service.impl;

import org.example.dao.INewsDAO;
import org.example.model.NewsModel;
import org.example.paging.Pageable;
import org.example.service.INewsService;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;

public class NewsService implements INewsService {
    @Inject
    private INewsDAO newsDAO;

    @Override
    public List<NewsModel> findAll(Pageable pageable) {
        return newsDAO.findAll(pageable);
    }

    @Override
    public List<NewsModel> findByCategoryId(Long categoryId) {
        return newsDAO.findByCategoryId(categoryId);
    }

    @Override
    public NewsModel save(NewsModel newsModel) {
        newsModel.setCreatedBy("");
        newsModel.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        Long id = newsDAO.save(newsModel);
        return newsDAO.findOne(id);
    }

    @Override
    public NewsModel update(NewsModel updatedNews) {
        NewsModel oldNews = newsDAO.findOne(updatedNews.getId());
        updatedNews.setCreatedBy(oldNews.getCreatedBy());
        updatedNews.setCreatedDate(oldNews.getCreatedDate());
        updatedNews.setModifiedBy("");
        updatedNews.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        newsDAO.update(updatedNews);
        return newsDAO.findOne(updatedNews.getId());
    }

    @Override
    public void delete(long[] ids) {
        for (long id: ids) {
            newsDAO.delete(id);
        }
    }

    @Override
    public int getTotalItems() {
        return newsDAO.getTotalItems();
    }
}
