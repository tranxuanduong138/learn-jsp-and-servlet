package org.example.service;

import org.example.model.NewsModel;
import org.example.paging.Pageable;

import java.util.List;

public interface INewsService {
    List<NewsModel> findAll(Pageable pageable);
    List<NewsModel> findByCategoryId(Long categoryId);
    NewsModel save(NewsModel newsModel);
    NewsModel update(NewsModel updatedNews);
    void delete(long[] ids);
    int getTotalItems();
}
