package org.example.controller.admin;

import org.example.constant.SystemConstant;
import org.example.model.NewsModel;
import org.example.paging.PageRequest;
import org.example.paging.Pageable;
import org.example.service.INewsService;
import org.example.sort.Sorter;
import org.example.utils.FormUtil;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "NewsController", urlPatterns = {"/admin-news"})
public class NewsController extends HttpServlet {
    @Inject
    INewsService newsService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NewsModel model = FormUtil.toModel(NewsModel.class, request);
        Pageable pageable = new PageRequest(model.getCurrentPage(),
                model.getItemsPerPage(),
                new Sorter(model.getSortName(), model.getSortBy()));
        model.setResultList(newsService.findAll(pageable));
        model.setTotalItems(newsService.getTotalItems());
        model.setTotalPages((int) Math.ceil((double) model.getTotalItems() / model.getItemsPerPage()));
        request.setAttribute(SystemConstant.MODEL, model);
        RequestDispatcher rd = request.getRequestDispatcher("/views/admin/news/list.jsp");
        rd.forward(request, response);
    }
}
