package org.example.controller.admin.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.model.NewsModel;
import org.example.service.INewsService;
import org.example.utils.HttpUtil;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "NewsAPI", urlPatterns = {"/api-admin-news"})
public class NewsAPI extends HttpServlet {
    @Inject
    private INewsService newsService;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        NewsModel newsModel = HttpUtil.of(request.getReader()).toModel(NewsModel.class);
        newsModel = newsService.save(newsModel);

        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getOutputStream(), newsModel);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        NewsModel updatedNews = HttpUtil.of(request.getReader()).toModel(NewsModel.class);
        updatedNews = newsService.update(updatedNews);

        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getOutputStream(), updatedNews);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        NewsModel newsModel = HttpUtil.of(request.getReader()).toModel(NewsModel.class);
        newsService.delete(newsModel.getIds());

        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getOutputStream(), "{}");
    }
}
