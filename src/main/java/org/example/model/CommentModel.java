package org.example.model;

public class CommentModel extends AbstractModel<CommentModel> {
    private String content;
    private long userId;
    private long newsIt;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getNewsIt() {
        return newsIt;
    }

    public void setNewsIt(long newsIt) {
        this.newsIt = newsIt;
    }
}
