package org.example.paging;

import org.example.sort.Sorter;

public class PageRequest implements Pageable {
    private Integer currentPage;
    private Integer itemsPerPage;
    private Sorter sorter;

    public PageRequest(Integer currentPage, Integer itemsPerPage, Sorter sorter) {
        this.currentPage = currentPage;
        this.itemsPerPage = itemsPerPage;
        this.sorter = sorter;
    }

    @Override
    public Integer getPage() {
        return currentPage;
    }

    @Override
    public Integer getOffset() {
        if (currentPage != null && itemsPerPage != null) {
            return (currentPage - 1) * itemsPerPage;
        }
        return null;
    }

    @Override
    public Integer getLimit() {
        return itemsPerPage;
    }

    public Sorter getSorter() {
        return sorter;
    }
}
